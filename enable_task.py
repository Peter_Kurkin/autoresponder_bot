import subprocess
import shutil
import os

from dotenv import load_dotenv

load_dotenv()
json_name = os.environ.get('json_name')

# Устанавливаем кодировку
os.system('chcp 65001')

# Задаем имя задачи
task_name = "TelegramBot"

# Останавливаем задачу, если задача уже запущена
os.system(f"schtasks /End /TN {task_name}")

# Запускаем pyinstaller
pyinstaller_command = f'pyinstaller --onefile --add-data ".env;." --add-data "{json_name};." main.py'
print(pyinstaller_command)
subprocess.run(pyinstaller_command, shell=True, check=True)

# Переносим файл
shutil.move('dist/main.exe', 'exe/main.exe')

# Запускаем задачу
schtasks_command = f"schtasks /Run /TN {task_name}"
subprocess.run(schtasks_command, shell=True, check=True)
