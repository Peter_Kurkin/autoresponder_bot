import json
import os
import chats_data
from dotenv import load_dotenv
from telethon import TelegramClient, events
from loguru import logger

# Замените YOUR_API_ID и YOUR_API_HASH на ваши значения
load_dotenv()
api_id = os.environ.get('api_id')
api_hash = os.environ.get('api_hash')
json_name = os.environ.get('json_name')

logger.info('Подключение к Telegram...')
client = TelegramClient('Anon.session', api_id, api_hash, device_model="iPhone 55 Pro", system_version="IOS 17",
                        app_version='Telethon IOS 17.1')
logger.info('Успешно подключено к Telegram')

white_list = [data for data in chats_data.whitelist.values()]


@client.on(events.NewMessage(incoming=True))
async def my_event_handler(event):
    try:
        if event.is_private:
            user = await client.get_entity(event.message.sender_id)
            if user:
                logger.info(f"Имя пользователя: {user.first_name}")
                logger.info(f"Фамилия пользователя: {user.last_name}")
                logger.info(f"Username пользователя: {user.username}")
                logger.info(f"--------------------------------")
            else:
                logger.warning("Пользователь не найден.")

                # result = {
                #     'chat_id': event.chat_id,
                #     'date': current_time,
                #     'text': event.message.raw_text,
                #     'from_id': event.message.sender_id,
                #     'is_channel': event.is_channel,
                #     'is_group': event.is_group,
                #     'is_private': event.is_private,
                # }
            try:
                with open(json_name, 'r') as json_file:
                    data = json.load(json_file)
            except FileNotFoundError:
                data = {}

            if str(event.message.sender_id) not in data:
                data[str(event.message.sender_id)] = {
                    "username": user.username,
                    "name": user.first_name,
                    "surname": user.last_name,
                    "phone": user.phone,
                }

                with open(json_name, 'w', encoding='utf-8') as file:
                    json.dump(data, file, ensure_ascii=False, indent=4)
    except Exception as e:
        logger.error(f"Ошибка при поиске пользователя: {str(e)}")

    # if (not event.is_channel and not event.is_group) or (event.chat_id in white_list):
    if event.chat_id in white_list:
        if 'сделал' in event.message.raw_text.lower():
            await event.reply('не сделал')

        elif 'сделаешь' in event.message.raw_text.lower():
            await event.reply('не сделаю')

        elif 'скинешь' in event.message.raw_text.lower():
            await event.reply('Нет')

        elif 'скинь' in event.message.raw_text.lower():
            await event.reply('No')

        elif 'clown' in event.message.raw_text.lower():
            await event.reply('Сам клоун')

        elif 'клоун' in event.message.raw_text.lower():
            await event.reply('Сам клоун')

        elif 'соси' in event.message.raw_text.lower():
            await event.reply('эээээ')

        elif 'спишу' in event.message.raw_text.lower():
            await event.reply('Не спишешь')

        elif 'спокойной ночи' in event.message.raw_text.lower():
            await event.reply('Спокойной ночи!')

        elif 'доброе утро' in event.message.raw_text.lower():
            await event.reply('Доброе утро!')

        elif 'клаун' in event.message.raw_text.lower():
            await event.reply('Сам клоун')


with client:
    client.run_until_disconnected()
